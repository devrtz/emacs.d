;;; init.el --- Emacs Initialization File
;;; Commentary:
;;; Most of my Emacs'ing was done using Spacemacs
;;; and later I switched to Doom Emacs.
;;; While these got my started, I really want something of my own!
;;;
;;; Things I long for:
;;; - hjkl everywhere (evil-collection!)
;;; - magit
;;; - IDE like features for C
;;; - IDE like features for Python, Elisp
;;; - which-key is a must
;;; - Org mode
;;; - devhelp
;;; - major modes for all common filetypes
;;; - Nice to have: Email (mu4e)
;;
;; Copyright (c) 2023 Evangelos Ribeiro Tzaras
;;
;; Author: Evangelos Ribeiro Tzaras <devrtz@fortysixandtwo.eu>
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(setq user-full-name "Evangelos Ribeiro Tzaras"
      user-mail-address "devrtz@fortysixandtwo.eu")

;; set tramp terminal type and add the following to ~/.zshrc on remote hosts
;; [[ $TERM == "tramp" ]] && unsetopt zle && PS1='$ ' && return
;; to ensure tramp will not get confused by any custom prompts
(setq tramp-terminal-type "tramp")

;; don't clutter init.el with custom vars (e.g. allowed stuff from .dir-locals.el)
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; add all subprojects in ~/.emacs.d/subprojects to the load-path
(defun add-subdir-to-load-path (dir)
  (dolist (f (directory-files dir t directory-files-no-dot-files-regexp))
    (if (and (file-directory-p f)
	     (not (directory-empty-p f)))
	(unless (seq-contains-p load-path f)
	  (add-to-list 'load-path f)))))

(add-subdir-to-load-path (expand-file-name "subprojects" user-emacs-directory))

(setq inhibit-startup-message t)

(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(setq visible-bell t)
(setq word-wrap t)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(add-hook 'prog-mode-hook
	  (lambda () (setq-local show-trailing-whitespace 1)))

(column-number-mode)
(global-display-line-numbers-mode 1)

; display line numbers except when in the following modes
(dolist (mode '(org-mode-hook
		term-mode-hook
		eshell-mode-hook
		vterm-mode-hook
		Man-mode-hook
		help-mode-hook
		helpful-mode-hook
		ivy-occur-mode-hook
		ivy-occur-grep-mode-hook
		image-minor-mode-hook
		image-mode-hook
		dired-mode-hook
		doc-view-mode-hook
		pdf-view-mode-hook
		pdf-occur-buffer-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(require 'use-package)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package hl-todo
  :config (global-hl-todo-mode))

(use-package electric-pair
  :hook (prog-mode . electric-pair-mode))

(use-package editorconfig
  :diminish
  :config
  (editorconfig-mode 1))

(use-package org
  :bind (("C-c l" . #'org-store-link)
	 ("C-c a" . #'org-agenda)
	 ("C-c c" . #'org-capture))
  :config
  (setq org-startup-truncated nil))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode))

; scaled fonts
(dolist (face '((org-level-1 . 1.2)
		(org-level-2 . 1.1)
		(org-level-3 . 1.05)
		(org-level-4 . 1.0)
		(org-level-5 . 0.95)
		(org-level-6 . 0.95)
		(org-level-7 . 0.95)
		(org-level-8 . 0.95)))
  (set-face-attribute (car face) nil :weight 'regular :height (cdr face)))

(use-package yasnippet
  :config
  (yas-global-mode))

(use-package vterm
  :commands (vterm))

(use-package with-editor
  :hook (vterm-mode . with-editor-export-editor))

(use-package dired
  :commands (dired dired-jump dired-jump-other-window)
  :bind (("C-c j" . #'dired-jump)
	 ("C-c J" . #'dired-jump-other-window))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" #'dired-up-directory
    "l" #'dired-find-file
    (kbd "C-<return>") #'dired-find-file-other-window)
  (setq dired-listing-switches "-al --group-directories-first"
	dired-dwim-target t))

(use-package dired-x
  :after dired)

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("C-l" . ivy-alt-done)
	 ("C-j" . ivy-next-line)
	 ("C-k" . ivy-previous-line)
	 :map ivy-switch-buffer-map
	 ("C-k" . ivy-previous-line)
	 ("C-l" . ivy-done)
	 ("C-d" . ivy-switch-buffer-kill)
	 :map ivy-reverse-i-search-map
	 ("C-k" . ivy-previous-line)
	 ("C-d" . ivy-reverse-i-search-kill))
  :config
  (setq ivy-wrap t)
  (ivy-mode 1)
  (evil-make-overriding-map ivy-occur-mode-map 'normal)
  (evil-make-overriding-map ivy-occur-grep-mode-map 'normal))

(use-package ivy-rich
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 ("C-M-j" . counsel-switch-buffer)
	 :map minibuffer-local-map
	 ("C-r" . counsel-minibuffer-history))
  :config
  (ivy-rich-mode 1))

(use-package helpful
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key)
  ([remap apropos-command] . counsel-apropos)
  :init
  (setq counsel-describe-function-function #'helpful-callable)
  (setq counsel-describe-variable-function #'helpful-variable))

(use-package which-key
  :diminish
  :config
  (setq which-key-idle-delay 0.2)
  (which-key-mode))

(use-package doom-themes
  :config
  (load-theme 'doom-one t))

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode))

(use-package evil
  :init
  (setq evil-want-integration t
	evil-want-keybinding nil
	evil-want-C-u-scroll t
	evil-want-C-i-jump t
	evil-undo-system #'undo-redo)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") #'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") #'evil-delete-backward-char-and-join); maybe not?

  (evil-global-set-key 'motion "j" #'evil-next-visual-line)
  (evil-global-set-key 'motion "k" #'evil-previous-visual-line))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package general
  :config
  (general-create-definer devrtz/leader
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "C-SPC"
    :prefix-command 'devrtz-leader-prefix-command
    :prefix-map 'devrtz-leader-prefix-map)
  (general-create-definer devrtz/project
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC p"
    :global-prefix "C-SPC p"
    :prefix-command 'devrtz-project-prefix-command
    :prefix-map 'devrtz-project-prefix-map)
  (general-create-definer devrtz/code
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC c"
    :global-prefix "C-SPC c"
    :prefix-command 'devrtz-code-prefix-command
    :prefix-map 'devrtz-code-prefix-map)
   (general-create-definer devrtz/git
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC g"
    :global-prefix "C-SPC g"
    :prefix-command 'devrtz-git-prefix-command
    :prefix-map 'devrtz-git-prefix-map)
   (general-create-definer devrtz/open
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC o"
    :global-prefix "C-SPC o"
    :prefix-command 'devrtz-open-prefix-command
    :prefix-map 'devrtz-open-prefix-map)
   (general-create-definer devrtz/buffer
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC b"
    :global-prefix "C-SPC b"
    :prefix-command 'devrtz-buffer-prefix-command
    :prefix-map 'devrtz-buffer-prefix-map)
   (general-create-definer devrtz/help
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC h"
    :global-prefix "C-SPC h"
    :prefix-command 'devrtz-help-prefix-command
    :prefix-map 'devrtz-help-prefix-map)
   )

(use-package hydra)
(use-package ivy-hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(defun evil-window-vsplit/follow ()
  (interactive)
  (let ((evil-vsplit-window-right (not evil-vsplit-window-right)))
    (call-interactively #'evil-window-vsplit)))

(defun evil-window-split/follow ()
  (interactive)
  (let ((evil-split-window-below (not evil-split-window-below)))
    (call-interactively #'evil-window-split)))

(devrtz/leader
  "/" '(swiper :which-key "swiper")
  "." (general-simulate-key "C-x C-f" :which-key "find-file")
  "`" '(evil-switch-to-windows-last-buffer :which-key "last-buffer")
  ":" (general-simulate-key "M-x" :which-key "M-x")
  "u" '(universal-argument :which-key "C-u")
  "t" '(:ignore t :which-key "toggles")
  "tt" '(load-theme :which-key "theme")
  "ts" '(hydra-text-scale/body :which-key "scale text")
  "w" '(evil-window-map :which-key "windows")
  "wV" '(evil-window-vsplit/follow :which-key "follow vertical split")
  "wS" '(evil-window-split/follow :which-key "follow horizontal split")
  "?" '(which-key-show-top-level :which-key "show all bindings"))

(devrtz/open
 "t" '(vterm :which-key "vterm")
 "f" '(ffap :which-key "file at point")
 "d" '(dired :which-key "dired")
 "j" '(dired-jump :which-key "dired-jump")
 "J" '(dired-jump-other-window :which-key "dired-jump-other-window")
 "r" '(counsel-recentf :which-key "recent files"))

(devrtz/buffer
 "b" '(counsel-switch-buffer :which-key "switch buffer")
 "B" '(counsel-switch-buffer-other-window :which-key "switch other buffer")
 "k" '(kill-this-buffer :which-key "kill this buffer")
 "K" '(kill-buffer :which-key "kill-buffer"))

(devrtz/help
  "f" '(describe-function :which-key "describe function")
  "k" '(describe-key :which-key "describe key")
  "v" '(describe-variable :which-key "describe variable")
  "p" '(describe-package :which-key "describe package")
  "m" '(describe-mode :which-key "describe mode")
  "a" '(counsel-apropos :which-key "apropos")
  "M" '(man :which-key "man"))

(use-package projectile
  :diminish
  :config
  (projectile-mode)
  (add-to-list 'projectile-globally-ignored-directories "*^_build")
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (let ((dirs '("~/git"
		"~/git/salsa"
		"~/git/salsa/voip"
		"~/git/pureos"
		"~/.emacs.d/subprojects")))
    (setq projectile-project-search-path
	  (seq-filter #'file-directory-p dirs)))
  (setq projectile-switch-project-action #'projectile-dired
	projectile-completion-system 'ivy
	projectile-enable-caching t
	projectile-auto-discover nil)
  (devrtz/project
    "c" '(projectile-compile-project :which-key "compile")
    "D" '(projectile-discover-projects-in-search-path :which-key "discover in search path")
    "d" '(projectile-discover-projects-in-directory :which-key "discover in directory")
    "g" '(projectile-configure-project :which-key "configure")
    "f" '(projectile-find-file :which-key "file")
    "t" '(projectile-test-project :which-key "test")
    "s" '(projectile-grep :which-key "grep")
    "o" '(ff-find-other-file :which-key "other file")
    "p" '(projectile-switch-project :which-key "switch project")))

(use-package counsel-projectile
  :config
  (counsel-projectile-mode))

(use-package magit
  :demand
  :hook (git-commit-mode . evil-insert-state)
  :config
  (devrtz/git
   "g" '(magit-status :which-key "status")
   "c" '(magit-clone :which-key "clone")
   "b" '(magit-blame :which-key "blame")))

(use-package devhelp)

;; lsp-mode requires a build-commands.json to be found
;; (or pointed to via .clangd's CompilationDatabase config)
;; this should not be an issue, because you can always
;; fallback to "bear -- compilation_command" to
;; generate the json file
(use-package lsp-mode
  :hook (c-mode . lsp)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t)
  :general
  (:states 'normal :keymaps 'c-mode-map "gr" #'lsp-find-references))

(use-package lsp-ui
  :after lsp-mode
  :bind
  (:map lsp-ui-peek-mode-map
	("C-j" . #'lsp-ui-peek--select-next)
	("C-J" . #'lsp-ui-peek--select-next-file)
	("C-k" . #'lsp-ui-peek--select-prev)
	("C-K" . #'lsp-ui-peek--select-prev-file)
	("C-o" . #'lsp-ui-peek--goto-xref-other-window))
  :init
  (setq lsp-ui-doc-position 'bottom))

; TODO disable all formatting with lsp!
; and use uncrustify instead

(use-package company
  :hook (prog-mode . company-mode)
  :bind (("C-<tab>" . #'company-capf))
  :config
  (setq company-minimum-prefix-length 1
	company-idle-delay 0.0
	company-tooltip-align-annotations t
	company-selection-wrap-around t
	company-backends '(company-bbdb
			   company-semantic
			   company-xcode company-cmake
			   company-capf
			   company-clang
			   company-files
			   (company-dabbrev-code
			    company-gtags
			    company-etags
			    company-keywords)
			   company-oddmuse company-dabbrev))
					; taken from upstream commit 664dd19
					; we need company-capf before company-clang
					; otherwise not all completions are being picked up
  )

(defun devrtz/setup-uncrustify ()
  (unless (string-prefix-p (projectile-project-root) uncrustify-uncrustify-cfg-file)
    (let ((try-cfg-file (directory-files (projectile-project-root) t "uncrustify.cfg$" nil 1)))
      (if try-cfg-file
	  (setq-local uncrustify-uncrustify-cfg-file (car try-cfg-file))))))

; not very smart: rewrites the whole buffer
; so buffer can get marked as changed even though it's 100% identical
(use-package uncrustify
  :hook (c-mode . devrtz/setup-uncrustify)
  :demand) ; TODO format keybind in "code" map

(devrtz/code
 "d" '(evil-goto-definition :which-key "goto definition")
 "r" '(xref-find-references :which-key "find references")
 "p" '(:ignore t :which-key "peek")
 "pr" '(lsp-ui-peek--get-references :which-key "references at point")
 "pR" '(lsp-ui-peek-find-references :which-key "references")
 "pd" '(lsp-ui-peek-find-definitions :which-key "definition")
 "h" '(devhelp-word-at-point :which-key "devhelp")
 "f" '(uncrustify-buffer :which-key "format"))

; throws some error when trying to start process?
;(use-package rtags
;  :after company
;  :hook (c-mode . rtags-start-process-unless-running)
;  :config
;  (setq rtags-completions-enabled 1
;	rtags-display-result-backend 'ivy)
;  (push 'company-rtags company-backends))
; (use-package flycheck-rtags)

;; file type support
(use-package yaml-mode)
(use-package markdown-mode)
(use-package nxml-mode)
(use-package meson-mode)

;; misc

(use-package debian-bug)
(use-package apt-sources)
(use-package debview
  :hook (debview-mode . evil-emacs-state)
  :general
  (:states 'emacs
	   "j" #'evil-next-visual-line
	   "k" #'evil-previous-visual-line
	   "C-u" #'evil-scroll-up
	   "C-d" #'evil-scroll-down
	   "SPC" #'devrtz-leader-prefix-command))

(use-package pdf-tools
  :demand
  :general
  (:states 'normal :keymaps 'pdf-view-mode-map
	   "h" #'pdf-view-previous-page-command
	   "l" #'pdf-view-next-page-command
	   "G" #'pdf-view-last-page
	   "C-o" #'pdf-history-backward
	   "C-i" #'pdf-history-forward
	   "m" #'pdf-view-position-to-register
	   "'" #'pdf-view-jump-to-register
	   "/" #'pdf-occur
	   "o" #'pdf-outline
	   "f" #'pdf-links-action-perform
	   "b" #'pdf-view-midnight-minor-mode)
  :config
  (pdf-loader-install)
  (add-hook 'pdf-view-mode-hook #'pdf-view-fit-height-to-window))
